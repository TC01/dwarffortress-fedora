#!/bin/bash

# Make sure we see commands.
set -e

# Location of specs. This is still hardcoded.
if [ -z $SPECDIR ]; then
	SPECDIR=~bjr/rpmbuild/SPECS/
fi

# Location of sources. This, too, is still hardcoded.
if [ -z $SOURCEDIR ]; then
	SOURCEDIR=~bjr/rpmbuild/SOURCES/
fi

# Kerberos/AFS credentials.
if [ -z $KRB_PRINC ]; then
	KRB_PRINC=bjr@ACM.JHU.EDU
fi
if [ -z $AFS_CELL ]; then
	AFS_CELL=acm.jhu.edu
fi

# Start a GPG agent, load keys.
export GPG_TTY=$(tty)
gpg-connect-agent reloadagent /bye
echo | gpg2 -s >/dev/null

# Get relevant credentials, if we don't already have AFS credentials.
# Why is Kerberos so slow?
# (Maybe it has something to do with the fact that we use Beaglebones as our KDCs...)
# We're going to ignore the fact where you could have AFS tokens for the wrong user.
tokens_string=`tokens | grep "Expires"`
if [ -z "${tokens_string}" ]; then
	kinit -V ${KRB_PRINC}
	aklog -d ${AFS_CELL}
fi

# Function to build, sign RPMs for an architecture.
function build_rpms_for_arch {

	# Define local variables for arguments.
	local pkgname=$1
	local mockcfg=$2
	local target=$3
	local multiarch=$4

	# Temporary directory to store specs for signing before deploying them.
	local WORKINGDIR=`mktemp -d`

	# Extract the "short" version and arch from mock configuration string.
	IFS='-' read -ra ADDR <<< "$mockcfg"
	local distname=${ADDR[0]}
	local shortver=${ADDR[1]}
	local arch=${ADDR[2]}

	# Convert the distname to a distcode.
	if [[ $distname == "fedora" ]]; then
		local distcode="fc"${shortver}
	elif [[ $distname == "epel" ]]; then
		local distcode="el"${shortver}
	fi

	# Fix the i686 <- i386 thing.
	if [[ $arch == "i386" ]]; then
		local arch="i686"
	fi

	local spec=${SPECDIR}/${pkgname}.spec

	# Use mock to build a SRPM, copy it to WORKINGDIR.
	mock -r ${mockcfg} --spec=${spec} --sources=${SOURCEDIR} --buildsrpm
	cp -v /var/lib/mock/${mockcfg}/result/*.src.rpm ${WORKINGDIR}

	# Use mock to build binary RPM(s), copy them to WORKINGDIR.
	mock -r ${mockcfg} /var/lib/mock/${mockcfg}/result/${pkgname}*.src.rpm --no-clean
	cp -v /var/lib/mock/${mockcfg}/result/*.rpm ${WORKINGDIR}

	# Sign the RPMs of the relevant architecture and name in WORKINGDIR.
	if ls ${WORKINGDIR}/*.${arch}.rpm 1> /dev/null 2>&1; then
		rpm --addsign ${WORKINGDIR}/${pkgname}*${arch}.rpm
	fi
	if ls ${WORKINGDIR}/*.noarch.rpm 1> /dev/null 2>&1; then
		rpm --addsign ${WORKINGDIR}/${pkgname}*noarch.rpm
	fi

	# Deploy them into AFS. Make directories if they do not exist.
	mkdir -p ${target}/${shortver}/SRPMs/
	mkdir -p ${target}/${shortver}/${arch}/
	mkdir -p ${target}/${shortver}/${arch}/debug/

	mv -v ${WORKINGDIR}/${pkgname}*${distcode}.src.rpm ${target}/${shortver}/SRPMs/

	# Handle debuginfo RPMs specially.
	if ls ${WORKINGDIR}/*debug*.rpm 1> /dev/null 2>&1; then
		if [[ $multiarch != "" ]]; then
			cp -v ${WORKINGDIR}/${pkgname}*debug*${distcode}.${arch}.rpm ${target}/${shortver}/${multiarch}/debug/
		fi
		mv -v ${WORKINGDIR}/${pkgname}*debug*${distcode}.${arch}.rpm ${target}/${shortver}/${arch}/debug/
	fi
	# We assume no noarch debuginfo packages. This is probably a reasonable assumption.
#	mv -v ${WORKINGDIR}/${pkgname}*debug*${distcode}.noarch.rpm ${target}/${shortver}/${arch}/debug/

	if ls ${WORKINGDIR}/*.${arch}.rpm 1> /dev/null 2>&1; then
		if [[ $multiarch != "" ]]; then
			cp -v ${WORKINGDIR}/${pkgname}*${distcode}.${arch}.rpm ${target}/${shortver}/${multiarch}/
		fi
		mv -v ${WORKINGDIR}/${pkgname}*${distcode}.${arch}.rpm ${target}/${shortver}/${arch}/
	fi
	if ls ${WORKINGDIR}/*.noarch.rpm 1> /dev/null 2>&1; then
		if [[ $multiarch != "" ]]; then
			cp -v ${WORKINGDIR}/${pkgname}*${distcode}.noarch.rpm ${target}/${shortver}/${multiarch}/
		fi
		mv -v ${WORKINGDIR}/${pkgname}*${distcode}.noarch.rpm ${target}/${shortver}/${arch}/
	fi

	# Something we're going to gloss over here:
	# If a package *only* exists in 32-bit form it should be available in the 64-bit repo.
	# I'm not sure that can magically be detected.

	# Run createrepo.
	createrepo --update -d --deltas ${target}/${shortver}/${arch}/
	createrepo --update -d ${target}/${shortver}/SRPMs/

	if [[ $multiarch != "" ]]; then
		createrepo --update -d --deltas ${target}/${shortver}/${multiarch}/
	fi

	# Remove temporary directory when done!
	rm -rf ${WORKINGDIR}
}
