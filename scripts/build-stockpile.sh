#!/bin/bash

# List of architectures/OSes to build for.
MOCK_CFGS=(fedora-26-i386 fedora-26-x86_64)

# Package name.
PKG_NAME=stockpile

# AFS target path.
TARGET_PATH=/afs/acm.jhu.edu/user/bjr/acmsys/public_html/fedora/dwarffortress

# Set configuration: AFS and Kerberos credentials.
export KRB_PRINC=bjr@ACM.JHU.EDU
export AFS_CELL=acm.jhu.edu

# Set configuration: SPEC and SOURCE locations.
export SPECDIR=~bjr/Programming/fedora/dwarffortress/rpmfusion/
export SOURCEDIR=~bjr/rpmbuild/SOURCES/

# Source the file, above configuration options get passed to this script.
source ./build-afs-rpms.sh

# Download sources using spectool.
spectool -C ${SOURCEDIR}/ -g -f ${SPECDIR}/${PKG_NAME}.spec

# Build, push packages.
for i in ${MOCK_CFGS[@]}; do
	build_rpms_for_arch ${PKG_NAME} $i ${TARGET_PATH} ""
done
