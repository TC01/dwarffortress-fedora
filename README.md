# dwarffortress-fedora

This repository contains RPM spec files and buildscripts for maintaining
[Dwarf Fortress](http://www.bay12games.com/dwarves/) packages on Fedora
and related distros (for example, CentOS/RHEL and possibly also Mageia).

The ultimate goal of this initiative is to make playing Dwarf Fortress,
installing mods and user-made content like tilests, using third party applications
written to run alongside Dwarf Fortress, and developing said content, to
be as painless and well-supported as possible on Fedora.

See [here](https://www.acm.jhu.edu/~bjr/pages/dwarf-fortress-for-fedora.html) for more
information on this effort.

## buildscripts

This directory contains a series of shell scripts for building packages
for the [dwarffortress](https://www.acm.jhu.edu/~bjr/pages/dwarf-fortress-for-fedora.html)
repository.

These scripts exist in lieu of using a more complex method to build
and maintain packages.

## fedora

This directory contains specs eligible for submission to Fedora itself.
At the moment, that's the following:

* golang-github-milochristiansen-axis2
* golang-github-milochristiansen-lua

## rpmfusion

This directory contains specs that must be submitted to RPM Fusion instead
of Fedora. At the moment, that's the following:

* dwarffortress
* dwarftherapist
* dfhack
* dfhack-plugin-twbt
* stockpile
* rubble

## Legality

The specs and scripts in this repository, unless stated otherwise, are under
the terms of the MIT license (see LICENSE).

Dwarf Fortress is a proprietary (free as in beer but not as in speech) game
developed by Tarn Adams. If you like Dwarf Fortress, you should
[support its continued development!](http://www.bay12games.com/support.html)
