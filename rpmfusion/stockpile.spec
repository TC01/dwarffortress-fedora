# Created by pyp2rpm-3.2.1
%global pypi_name stockpile

%global mysterious_hash 619c3b6411a19ef8c8aa37324c58c2e6

Name:           %{pypi_name}
Version:        0.1.0
Release:        1%{?dist}
Summary:        Git-based mod manager for Dwarf Fortress

License:        MIT
URL:            https://gitlab.com/TC01/stockpile/
Source0:        https://gitlab.com/TC01/stockpile/uploads/%{mysterious_hash}/stockpile-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3-pygit2
Requires:       python3-setuptools

# Git is explicitly required.
Requires:       git

%description
Stockpile is a program to manage Dwarf Fortress installations using version
control. The goal is to allow a user to install DF once, create a git
repository in that installation, and then install mods and tilesets on
various branches of the repository. When a user wants to switch to a
different mod, they simply switch the branch, and then any launcher or
script that they have in place to launch their copy of DF will now run
on top of the other branch.

It was specifically designed to integrate well with the Fedora packages
of Dwarf Fortress-- packages where a single copy of all the data and raw
files are installed on the root filesystem and symbolically linked into
each user's home directory. When a branch is created on a user's DF
installation using stockpile, all symbolic links are collapsed so that
raw files can be modified locally.

This means that mods can be installed per user, do not require root
to install, and will not be overwritten by updates to the system packages.

%prep
%autosetup -n %{pypi_name}-%{version}
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install
cp %{buildroot}/%{_bindir}/stockpile %{buildroot}/%{_bindir}/stockpile-3
ln -sf %{_bindir}/stockpile-3 %{buildroot}/%{_bindir}/stockpile-%{python3_version}

#%check
# There's currently support to run tests, but no tests.
#%{__python3} setup.py test

%files
%license LICENSE
%doc README.md
%{_bindir}/stockpile
%{_bindir}/stockpile-3
%{_bindir}/stockpile-%{python3_version}
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py?.?.egg-info

%changelog
* Sat Jan 14 2017 Ben Rosser <rosser.bjr@gmail.com> - 0.1.0-1
- Initial package.
