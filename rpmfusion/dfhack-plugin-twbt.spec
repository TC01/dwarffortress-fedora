Name:           dfhack-plugin-twbt
Version:        5.70
Release:        1%{?dist}
Summary:        Plugin for dfhack that improves game interface

# as per https://github.com/mifki/df-twbt/issues/47, license file currently missing.
License:        zlib
URL:            https://github.com/mifki/df-twbt
Source0:        https://github.com/mifki/df-twbt/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

# Buildsystem patch.
Patch0:         https://www.acm.jhu.edu/~bjr/fedora/dwarffortress/rpmfusion/twbt-makefile-pkgconfig.patch

BuildRequires:  dfhack-devel, glew-devel
Requires:       dwarffortress

%description
This is a plugin for Dwarf Fortress / DFHack that improves various
aspects of the game interface.

%prep
%autosetup -n df-twbt-%{version}

%build
LDFLAGS="%{__global_ldflags}" CXXFLAGS="%{optflags}" %make_build

%install
mkdir -p %{buildroot}%{_libdir}/dfhack/hack/plugins/
mkdir -p %{buildroot}%{_libdir}/dfhack/hack/scripts/
make inst DF=%{buildroot}%{_libdir}/dfhack/
cp dist/realcolors.lua %{buildroot}%{_libdir}/dfhack/hack/scripts/

%files
%{_libdir}/dfhack/hack/plugins/twbt.plug.so
%{_libdir}/dfhack/hack/scripts/realcolors.lua
%doc README.md

%changelog
* Fri Jan 20 2017 Ben Rosser <rosser.bjr@gmail.com> - 5.70-1
- Update to latest upstream release.

* Sun Oct  2 2016 Ben Rosser <rosser.bjr@gmail.com> - 5.65-1
- Initial package, license missing.
