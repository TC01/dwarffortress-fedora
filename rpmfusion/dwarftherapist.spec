# On CentOS, we want to strip the ".centos" bit of the dist tag.
# Credit to https://serverfault.com/questions/688485/rpm-dist-tag-not-behaving-as-documented
# This should go away when the package is imported to RPM Fusion
%if 0%{?rhel} == 7
	%define dist .el7
%endif

Name:           dwarftherapist
Version:        39.2.0
Release:        1%{?dist}
Summary:        Management tool designed to run side-by-side with Dwarf Fortress

License:        MIT
URL:            https://github.com/Dwarf-Therapist/Dwarf-Therapist
Source0:        https://github.com/Dwarf-Therapist/Dwarf-Therapist/archive/v%{version}/dwarftherapist-%{version}.tar.gz

# Need cmake.
BuildRequires:  cmake

# Qt5 dependencies.
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtdeclarative-devel

# Desktop file utils.
BuildRequires:  desktop-file-utils

# dos2unix, for fixing a file.
BuildRequires:  dos2unix

# Required for post step.
Requires:       /usr/sbin/setcap

# We need Dwarf Fortress for Therapist to be useful! Therefore
# we should only build on x86_64 and i686 as these are the only
# arches DF exists on.
Requires:       dwarffortress
ExclusiveArch:  x86_64 i686

%description
Management tool designed to run side-by-side with Dwarf Fortress.
Offers several views and interface improvements to Dwarf Fortress.
Some features include:

Persistent custom professions - import and manage any number of custom
professions across all your forts.

Assign multiple dwarves to a custom profession at once to unify active labors

Manage labors and professions much more easily than in-game using a flexible
UI, allowing quick review of all dwarves at-a-glance

Display all pending changes before they're written to the game

Sort labor columns by associated skill level

Persistent and customizable display; change colors, reposition/hide
information screens

Group your dwarves by several criteria

This is a heavily modified version of the original Dwarf Therapist that
is still maintained for new versions of Dwarf Fortress.

%prep
%setup -qn Dwarf-Therapist-%{version}
dos2unix CHANGELOG.txt

%build
%cmake
%make_build

%install
%make_install

# Check the desktop file.
desktop-file-validate %{buildroot}/%{_datadir}/applications/dwarftherapist.desktop

# Install the resources?
cp -rp resources/* %{buildroot}%{_datadir}/dwarftherapist/
mkdir -p %{buildroot}%{_datadir}/pixmaps
cp -p resources/img/*.xpm %{buildroot}%{_datadir}/pixmaps/

# Remove an extra copy of the license.
rm %{buildroot}%{_docdir}/dwarftherapist/LICENSE.txt

# There used to be a link from /usr/bin/dwarftherapist -> /usr/bin/DwarfTherapist
# (or a wrapper script or something). Create one manually.
ln -s %{_bindir}/DwarfTherapist %{buildroot}%{_bindir}/dwarftherapist

# Post install script; set cap permissions.
%post
sudo setcap cap_sys_ptrace=eip %{_bindir}/DwarfTherapist

%files
%{_bindir}/DwarfTherapist
%{_bindir}/dwarftherapist
%{_datadir}/dwarftherapist
%{_datadir}/applications/dwarftherapist.desktop
%{_datadir}/pixmaps/dwarftherapist.*
%{_datadir}/icons/hicolor/*/apps/dwarftherapist.png
%doc README.rst CHANGELOG.txt
%license LICENSE.txt


%changelog
* Tue Jan 30 2018 Ben Rosser <rosser.bjr@gmail.com> - 39.2.0-1
- Update to new upstream, with new release supporting new DF releases.

* Sat Jul 16 2016 Ben Rosser <rosser.bjr@gmail.com> - 37.0.0-4
- Add newer memory layouts from git for 0.43.02, 0.43.03.
- Compatibility with EL7; this just requires disabling compiling the documentation.

* Fri Jun 17 2016 Ben Rosser <rosser.bjr@gmail.com> - 37.0.0-3
- Arched dependency on dwarffortress is unnecessary.

* Thu May 19 2016 Ben Rosser <rosser.bjr@gmail.com> - 37.0.0-2
- Fix line endings on the changelog file.
- Slightly reword description.
- Build latex documentation multiple times to get cross-references correct.

* Wed May 18 2016 Ben Rosser <rosser.bjr@gmail.com> - 37.0.0-1
- Initial package.
