# This package is now maintained in Fedora, and can be found at:
# http://pkgs.fedoraproject.org/cgit/rpms/legendsbrowser.git/

Name:           legendsbrowser
Version:        1.0.12
Release:        1%{?dist}
Summary:        Java-based legends viewer for Dwarf Fortress

License:        MIT
URL:            https://github.com/robertjanetzko/LegendsBrowser

Source0:        https://github.com/robertjanetzko/LegendsBrowser/archive/%{version}.tar.gz

# This is a Java package.
BuildArch:      noarch

BuildRequires:  maven-local, javapackages-tools

# Dependencies.
Requires:       dom4j
Requires:       junit
Requires:       guava
Requires:       reflections
Requires:       velocity
Requires:       apache-commons-logging, apache-commons-lang, apache-commons-collections, apache-commons-cli

BuildRequires:  dom4j, junit, guava, velocity, apache-commons-logging, javassist
BuildRequires:  reflections

# Javadoc package.
%package       javadoc
Summary:       Javadoc for %{name}

%description javadoc
This package contains the API documentation for %{name}.

%description
Legends Browser is an multi-platform, open source, java-based legends viewer
for Dwarf Fortress. It works in the browser of your choice and recreates Legends
Mode, with objects accessible as links. Several statistics and overviews are
added.

%prep
%setup -qn LegendsBrowser-%{version}

# Remove irrelevant Maven plugins-- I think.
%pom_remove_plugin :reflections-maven
%pom_remove_plugin :maven-shade-plugin
%pom_remove_plugin :launch4j-maven-plugin
%pom_remove_plugin :maven-jar-plugin

%build
%mvn_build

%install
%mvn_install

# Install a script to run the application.
# ...we hopefully don't need more dependencies than this...
# There must be some way to make this multiline?
%jpackage_script legends.Application "" "" legendsbrowser:reflections:velocity:commons-logging:guava:dom4j:apache-commons-collections:apache-commons-logging:javassist:apache-commons-lang:apache-commons-cli legendsbrowser true

%files -f .mfiles
%{_bindir}/legendsbrowser
%dir %{_javadir}/%{name}
%doc README.md
%license LICENSE.md

%files javadoc -f .mfiles-javadoc

%changelog
* Sat Jul 23 2016 Ben Rosser <rosser.bjr@gmail.com> 1.0.12-1
- Update to latest upstream version

* Fri Jun 17 2016 Ben Rosser <rosser.bjr@gmail.com> 1.0.11-2
- Whoops, typo in jpackage_script; main class is not .main.
- Missing apache-commons-lang dependency in jpackage script.

* Fri May 27 2016 Ben Rosser <rosser.bjr@gmail.com> 1.0.11-1
- Initial package.
